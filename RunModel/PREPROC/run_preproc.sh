#!/bin/bash
# ---------------------------------------------------- #
# Script para o pre-processamento do WRF usando o GFS 
#
# Ronaldo Palmeira & Marcelo Romero - dez/2017
# ---------------------------------------------------- #

ANO=$1
MES=$2
DIA=$3
HOR=$4
FCST=$5

ANOF=`date +%Y --d "$ANO$MES$DIA $HOR +$FCST hours"`
MESF=`date +%m --d "$ANO$MES$DIA $HOR +$FCST hours"`
DIAF=`date +%d --d "$ANO$MES$DIA $HOR +$FCST hours"`
HORF=`date +%H --d "$ANO$MES$DIA $HOR +$FCST hours"`

MDIR="/home/atmosmarine/PNT/RunModel/PREPROC"
cd $MDIR

rm -f GRIBFILE.A* met_em.d0* FILE*

cat > namelist.wps << EOF
&share
 wrf_core = 'ARW',
 max_dom = 2,
 start_date = '${ANO}-${MES}-${DIA}_${HOR}:00:00','${ANO}-${MES}-${DIA}_${HOR}:00:00',
 end_date   = '${ANOF}-${MESF}-${DIAF}_${HORF}:00:00','${ANOF}-${MESF}-${DIAF}_${HORF}:00:00',
 interval_seconds = 21600
 io_form_geogrid = 2,
/

&geogrid
 parent_id         =   1,   1,
 parent_grid_ratio =   1,   3,
 i_parent_start    =   1,  90,
 j_parent_start    =   1, 101,
 e_we              = 236, 187,
 e_sn              = 186, 208,
 dx = 9000,
 dy = 9000,
 map_proj = 'lambert',
 ref_lat   = -23.30,
 ref_lon   = -40.50,
 truelat1  = -10.00,
 truelat2  = -50.00,
 stand_lon = -40.50,
 ! The default datasets used to produce the HGT_M, GREENFRAC, 
 ! and LU_INDEX/LANDUSEF fields have changed in WPS v3.8. The HGT_M field
 ! is now interpolated from 30-arc-second USGS GMTED2010, the GREENFRAC 
 ! field is interpolated from MODIS FPAR, and the LU_INDEX/LANDUSEF fields 
 ! are interpolated from 21-class MODIS.
 !
 ! To match the output given by the default namelist.wps in WPS v3.7.1, 
 ! the following setting for geog_data_res may be used:
 !
 ! geog_data_res = 'gtopo_10m+usgs_10m+nesdis_greenfrac+10m','gtopo_2m+usgs_2m+nesdis_greenfrac+2m',
 !
 !!!!!!!!!!!!!!!!!!!!!!!!!!!! IMPORTANT NOTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !
 geog_data_res = '30s','30s',
 geog_data_path = '/home/atmosmarine/PNT/WRFV3/WPS/geog'
/

&ungrib
 out_format = 'WPS',
 prefix = 'FILE',
/

&metgrid
 fg_name = 'FILE'
 io_form_metgrid = 2, 
 opt_output_from_metgrid_path = './',
 opt_metgrid_tbl_path = './',
/
EOF


./link_grib.csh /home/atmosmarine/PNT/RunModel/GFS/gfs.$ANO$MES$DIA$HOR/

./ungrib.exe

./metgrid.exe
