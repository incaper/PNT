#!/bin/bash
# ---------------------------------------------------- #
# Script principal para rodar o WRF usando o GFS 
#
# Ronaldo Palmeira & Marcelo Romero - dez/2017
# ---------------------------------------------------- #

ANO=`date +%Y`
MES=`date +%m`
DIA=`date +%d`
HOR=$1

MDIR="/home/atmosmarine/PNT/RunModel/"
cd $MDIR

# Rodada 1-7 dias
FCST=168 # horas de previsao

cd $MDIR/GFS/
./get_gfs_nomads.sh $ANO $MES $DIA $HOR $FCST > get_gfs_nomads.log 2>&1

cd $MDIR/PREPROC/
./run_preproc.sh $ANO $MES $DIA $HOR $FCST > run_preproc.log 2>&1

cd $MDIR/RUN/
./run_model.sh $ANO $MES $DIA $HOR $FCST > run_model.log 2>&1

cd $MDIR/POSPROC/
./run_posproc.sh $ANO $MES $DIA $HOR $FCST > run_posproc.log 2>&1


# Rodada 7-15 dias (com apenas 1 grade)
FCST1=360

cd $MDIR/GFS/
./get_gfs_nomads15.sh $ANO $MES $DIA $HOR $FCST $FCST1 > get_gfs_nomads15.log 2>&1

cd $MDIR/PREPROC/
./run_preproc15.sh $ANO $MES $DIA $HOR $FCST $FCST1 > run_preproc15.log 2>&1

cd $MDIR/RUN/
./run_model15.sh $ANO $MES $DIA $HOR $FCST $FCST1 > run_model15.log 2>&1

cd $MDIR/POSPROC/
./run_posproc15.sh $ANO $MES $DIA $HOR $FCST $FCST1 > run_posproc15.log 2>&1
