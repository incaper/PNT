#!/bin/bash
# ---------------------------------------------------- #
# Script para o Processamento do WRF 
#
# Ronaldo Palmeira & Marcelo Romero - dez/2017
# ---------------------------------------------------- #

ANO=$1
MES=$2
DIA=$3
HOR=$4
FCST=$5

ANOF=`date +%Y --d "$ANO$MES$DIA $HOR +$FCST hours"`
MESF=`date +%m --d "$ANO$MES$DIA $HOR +$FCST hours"`
DIAF=`date +%d --d "$ANO$MES$DIA $HOR +$FCST hours"`
HORF=`date +%H --d "$ANO$MES$DIA $HOR +$FCST hours"`

MPICH="/home/atmosmarine/tools/local/mpich/bin/"
MDIR="/home/atmosmarine/PNT/RunModel/RUN"

cd $MDIR

# Limpa diretorio das rodadas anteriores
rm -f wrfinput_d* wrfbdy_d* met_em.d* rsl.*

cat > namelist.input << EOF
 &time_control
 run_days                            = 0,
 run_hours                           = 0,
 run_minutes                         = 0,
 run_seconds                         = 0,
 start_year                          = ${ANO}, ${ANO}, ${ANO},
 start_month                         = ${MES}, ${MES}, ${MES},
 start_day                           = ${DIA}, ${DIA}, ${DIA},
 start_hour                          = ${HOR}, ${HOR}, ${HOR},
 start_minute                        = 00,   00,   00,
 start_second                        = 00,   00,   00,
 end_year                            = ${ANOF}, ${ANOF}, ${ANOF},
 end_month                           = ${MESF}, ${MESF}, ${MESF},
 end_day                             = ${DIAF}, ${DIAF}, ${DIAF},
 end_hour                            = ${HORF}, ${HORF}, ${HORF},
 end_minute                          = 00,   00,   00,
 end_second                          = 00,   00,   00,
 interval_seconds                    = 21600
 input_from_file                     = .true.,.true.,.true.,
 history_interval                    = 60,  60,   60,
 frames_per_outfile                  = 1000, 1000, 1000,
 restart                             = .false.,
 restart_interval                    = 1440,
 diag_print                          = 2,
 io_form_history                     = 2
 io_form_restart                     = 2
 io_form_input                       = 2
 io_form_boundary                    = 2
 debug_level                         = 0
 /

 &domains
 time_step                           = 54,
 time_step_fract_num                 = 0,
 time_step_fract_den                 = 1,
 use_adaptive_time_step              = .true.,
 step_to_output_time                 = .true.,
 target_cfl                          = 1.2,1.2,
 target_hcfl                         = .84,.84,
 max_step_increase_pct               = 5,51,   
 starting_time_step                  = -1,-1,  
 max_time_step                       = -1,-1,  
 min_time_step                       = -1,-1,  
 adaptation_domain                   = 1,      
 max_dom                             = 2,
 e_we                                = 236,    187,   94,
 e_sn                                = 186,    208,    91,
 e_vert                              = 45,    45,    30,
 p_top_requested                     = 5000,
 num_metgrid_levels                  = 32,
 num_metgrid_soil_levels             = 4,
 dx                                  = 9000, 3000,  3333.33,
 dy                                  = 9000, 3000,  3333.33,
 grid_id                             = 1,     2,     3,
 parent_id                           = 0,     1,     2,
 i_parent_start                      = 1,    90,    30,
 j_parent_start                      = 1,    101,    30,
 parent_grid_ratio                   = 1,     3,     3,
 parent_time_step_ratio              = 1,     3,     3,
 feedback                            = 1,
 smooth_option                       = 0
 /

 &physics
 mp_physics                          = 3,     3,     3,
 ra_lw_physics                       = 1,     1,     1,
 ra_sw_physics                       = 1,     1,     1,
 radt                                = 9,     9,    30,
 sf_sfclay_physics                   = 1,     1,     1,
 sf_surface_physics                  = 4,     4,     2,
 bl_pbl_physics                      = 1,     1,     1,
 bldt                                = 0,     0,     0,
 cu_physics                          = 2,     0,     0,
 cudt                                = 5,     5,     5,
 isfflx                              = 1,
 ifsnow                              = 1,
 icloud                              = 1,
 surface_input_source                = 3,
 num_soil_layers                     = 4,
 num_land_cat                        = 21,
 sf_urban_physics                    = 0,     0,     0,
 /

 &noah_mp
 dveg                               = 4,
 opt_crs                            = 1,
 opt_sfc                            = 1,
 opt_btr                            = 1,
 opt_run                            = 1,
 opt_frz                            = 1,
 opt_inf                            = 1,
 opt_rad                            = 3,
 opt_alb                            = 2,
 opt_snf                            = 1,
 opt_tbot                           = 2,
 opt_stc                            = 1,
 opt_gla                            = 1,
 opt_rsf                            = 1,
 /

 &fdda
 /

 &dynamics
 w_damping                           = 1,
 diff_opt                            = 1,      1,      1,
 km_opt                              = 4,      4,      4,
 diff_6th_opt                        = 0,      0,      0,
 diff_6th_factor                     = 0.12,   0.12,   0.12,
 base_temp                           = 290.
 damp_opt                            = 3,
 zdamp                               = 5000.,  5000.,  5000.,
 dampcoef                            = 0.2,    0.2,    0.2
 khdif                               = 0,      0,      0,
 kvdif                               = 0,      0,      0,
 epssm                               = 0.9,
 non_hydrostatic                     = .true., .true., .true.,
 moist_adv_opt                       = 1,      1,      1,     
 scalar_adv_opt                      = 1,      1,      1,     
 /

 &bdy_control
 spec_bdy_width                      = 5,
 spec_zone                           = 1,
 relax_zone                          = 4,
 specified                           = .true., .false.,.false.,
 nested                              = .false., .true., .true.,
 /

 &grib2
 /

 &namelist_quilt
 nio_tasks_per_group = 0,
 nio_groups = 1,
 /
EOF


#

ln -sf ../PREPROC/met_em.d0* .

$MPICH/mpirun -np 46 ./real.exe

$MPICH/mpirun -np 46 ./wrf.exe

# Remove arquivos de output com mais de 5 dias
find wrfout_d* -ctime +5 -type f | xargs rm -rf
