![atmosmarine.com](https://tlgur.com/d/0Gomy7b4)

# Sistema de Modelagem Numérica de Tempo Regional (PNT)

Operacionalização do modelo atmosférico Weather Research and Forecasting (WRF), versão 3.8.1, nos servidores do Incaper.

###RunModel/
Este diretorio contém todo o processamento do modelo WRF para 7 e 15 dias. O conteúdo do diretório segue o padrão abaixo e finalidade de cada uma das subpastas será descrita a seguir.
```
atmosmarine@srvwrf04:~/PNT$ ls -l RunModel/
total 64
drwxrwxr-x 55 atmosmarine atmosmarine  4096 Fev 15 15:00 GFS
drwxrwxr-x  2 atmosmarine atmosmarine  4096 Jan  3 18:56 GRADE
-rwxrwxr-x  1 atmosmarine atmosmarine   903 Fev 14 01:41 main_run_wrf.sh
drwxrwxr-x  3 atmosmarine atmosmarine 16384 Fev 15 21:46 POSPROC
drwxrwxr-x  2 atmosmarine atmosmarine 12288 Fev 15 15:29 PREPROC
-rw-rw-r--  1 atmosmarine atmosmarine  4949 Fev 15 21:50 README.md
drwxrwxr-x  2 atmosmarine atmosmarine 16384 Fev 15 19:02 RUN
```

### Crontab:
O crontab possui somente duas chamadas para execução de toda simulação das 00 e 12 (UTC) às 03h e 15 (BRST) respectivamente.
```
# m h  dom mon dow   command
SHELL=/bin/bash
#
00 03 * * * /home/atmosmarine/PNT/RunModel/main_run_wrf.sh 00 > /home/atmosmarine/PNT/RunModel/main_run_wrf00.log 2>&1
00 15 * * * /home/atmosmarine/PNT/RunModel/main_run_wrf.sh 12 > /home/atmosmarine/PNT/RunModel/main_run_wrf12.log 2>&1
```

Script ***main_run_wrf.sh***
Este é o script principal que aciona todo a cadeia de processos necessários para execução da simulação do WRF (7 e 15 dias).

```
#!/bin/bash
# ---------------------------------------------------- #
# Script principal para rodar o WRF usando o GFS 
#
# Ronaldo Palmeira & Marcelo Romero - dez/2017
# ---------------------------------------------------- #

ANO=`date +%Y`
MES=`date +%m`
DIA=`date +%d`
HOR=$1

MDIR="/home/atmosmarine/PNT/RunModel/"
cd $MDIR

# Rodada 1-7 dias
FCST=168 # horas de previsao

cd $MDIR/GFS/
./get_gfs_nomads.sh $ANO $MES $DIA $HOR $FCST > get_gfs_nomads.log 2>&1

cd $MDIR/PREPROC/
./run_preproc.sh $ANO $MES $DIA $HOR $FCST > run_preproc.log 2>&1

cd $MDIR/RUN/
./run_model.sh $ANO $MES $DIA $HOR $FCST > run_model.log 2>&1

cd $MDIR/POSPROC/
./run_posproc.sh $ANO $MES $DIA $HOR $FCST > run_posproc.log 2>&1


# Rodada 7-15 dias (com apenas 1 grade)
FCST1=360

cd $MDIR/GFS/
./get_gfs_nomads15.sh $ANO $MES $DIA $HOR $FCST $FCST1 > get_gfs_nomads15.log 2>&1

cd $MDIR/PREPROC/
./run_preproc15.sh $ANO $MES $DIA $HOR $FCST $FCST1 > run_preproc15.log 2>&1

cd $MDIR/RUN/
./run_model15.sh $ANO $MES $DIA $HOR $FCST $FCST1 > run_model15.log 2>&1

cd $MDIR/POSPROC/
./run_posproc15.sh $ANO $MES $DIA $HOR $FCST $FCST1 > run_posproc15.log 2>&1
```



**GFS/**

a. Shell-script que baixa previsões GFS (0.25): /home/atmosmarine/PNT/GFS/get_gfs_nomads.sh
b. tempo aproximado de download GFS (192 horas): ~ 5 min

**GRADE/**

A configuração deste projeto possui 2 Domínios, com 9 e 3km centrado no ES como mostra a figura abaixo:

![](https://tlgur.com/d/X8eAeNN8) 


**PREPROC/**
O pré-processamento do modelo possui 3 etapas básicas representados pelos programas geogrid.exe, ungrib.exe e metgrid.exe.
O geogrid.exe é rodado uma única vez quando da definição do domínio e resolução do modelo.
O processamento dos dados do GFS para que sirvam como condição inicial e condição de contorno do WRF é realizado antes de cada simulação.
Existem dois scripts (*run_preproc.sh* e *run_preproc15.sh*) para a execução do pré processamento diário do GFS  que gera as condições iniciais e de contorno das simulações de 7 e 15 dias.
Estes scripts acionam os programs do WPS *ungrib.exe*  que decodifica os arquivos grib2 e o *metgrid.exe* que interpola espacial e verticalmente os dados do GFS para as grades do WRF.

**RUN/**
Nesta pasta é onde o modelo que foi compilado na pasta PNT/WRFV3 é rodado. Nela, encontram-se as rotinas *run_model.sh* e *run_model15.sh* para disparar a simulação de 7 e 15 dias respectivamente.
Por questões de razoabilidade a simulação de 15 dias possui somente a grade 1 de 9km de resolução ativada.
Ao final script está a seleção de número de cores que serão usados na simulação.
A simulação é dividia duas etapas através dos executáveis real.exe e wrf.exe. O primeiro (real.exe) cria, através das saídas do WPS, os arquivos de condição inicial e de fronteira (*wrfinput_d01(2)* e *wrfbdy_d01(2)*) e o segundo (wrf.exe) executa a simulação pelo período de tempo selecionado.

** Tempo de Simulação WRF:**
Tempo de CPU das previsões com WRF para 000-168 horas (7dias) -> aprox. 230 min
Tempo de CPU das previsões com WRF para 168-360 horas (15 dias) -> aprox. 120 min

**POSPROC/**
Nesta pasta é executado o pós processamento do WRF através do pacote UPP do NCEP.
Este pacote possui a vantagem de ter o cálculo de inúmeras variáveis secundárias e diferentes possibilidade de níveis verticais. O principal arquivo de configuração do UPP é o wrf_cntrl.parm. Este arquivo definide quais serão os parametros do WRF que serão salvos e os respectiveis níveis verticais em hPa ou em outra coordenada. Para maiores detalhes verificar documentação do UPP [aqui.](https://dtcenter.org/upp/users/docs/user_guide/V3/upp_users_guide_binary.pdf) 
O wrf_cntrl.parm já está configurado com todas variáveis necessárias para este projeto.
Após toda simulação o script *run_posproc.sh* ou *run_)posproc15.sh* (para os 15 dias) roda o executável unipost.exe gerando arquivos no formato grib com os parâmetros solicitados.
Para a conversão final dos gribs em NetCDF (formato padrão adotado  neste projeto) é utilizado o software  GrADS e as rotinas lats4d.sh e lats4d.pl já no final do script *run_posproc.sh* ou *run_posproc15.sh*
Em uma última etapa são rodados 2 scripts em python (calcula_agromet.py e calcula_prevmet.py) onde são calculados parâmetros agrometeorológicos diários e os parãmetros do prevMET que servirão de base para o sistema de previsão sugerida pelo modelo.
