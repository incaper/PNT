dset ^WRFPRS.GrbF%f2
index ^WRFPRS.15dias.idx
undef 9.999E+20
title WRFPRS.15dias
*  produced by grib2ctl v0.9.13
* command line options: WRFPRS.15dias
dtype grib 255
options template
pdef 235 185 lccr -30.679000 307.755000 1 1 -50.000000 -10.000000 319.500000 9000 9000
xdef 235 linear -52.245 0.0951330692044615
ydef 185 linear -30.679000 0.0818181818181818
tdef 191 linear 13Z15Mar2018 1hr
*  z has 7 levels, for prs
zdef 7 levels
50 30 20 10 7 5 2
vars 36
ACPCPsfc  0 63,1,0  ** surface Convective precipitation [kg/m^2]
APCPsfc  0 61,1,0  ** surface Total precipitation [kg/m^2]
CAPEsfc  0 157,1,0  ** surface Convective Avail. Pot. Energy [J/kg]
CPRATsfc  0 214,1,0  ** surface Convective precip. rate [kg/m^2/s]
DLWRFsfc  0 205,1,0  ** surface Downward long wave flux [W/m^2]
DPT2m  0 17,105,2 ** 2 m above ground Dew point temp. [K]
DSWRFsfc  0 204,1,0  ** surface Downward short wave flux [W/m^2]
DSWRFtoa  0 204,8,0 ** top of atmos Downward short wave flux [W/m^2]
EVPsfc  0 57,1,0  ** surface Evaporation [kg/m^2]
GFLUXsfc  0 155,1,0  ** surface Ground heat flux [W/m^2]
GUSTsfc  0 180,1,0  ** surface Surface wind gust [m/s]
HCDChcl  0 75,234,0 ** high cloud level High level cloud cover [%]
HGTsfc  0 7,1,0  ** surface Geopotential height [gpm]
HGTprs 7 7,100,0 ** (profile) Geopotential height [gpm]
HINDEXsfc  0 250,1,0  ** surface Haines index []
LCDClcl  0 73,214,0 ** low cloud level Low level cloud cover [%]
LFTX500_1000mb  0 131,101,12900 ** 500-1000 mb Surface lifted index [K]
LHTFLsfc  0 121,1,0  ** surface Latent heat flux [W/m^2]
MCDCmcl  0 74,224,0 ** mid-cloud level Mid level cloud cover [%]
MSLETmsl  0 130,102,0 ** mean-sea level Mean sea level pressure (ETA model) [Pa]
PRATEsfc  0 59,1,0  ** surface Precipitation rate [kg/m^2/s]
PRESsfc  0 1,1,0  ** surface Pressure [Pa]
PRMSLmsl  0 2,102,0 ** mean-sea level Pressure reduced to MSL [Pa]
RH2m  0 52,105,2 ** 2 m above ground Relative humidity [%]
SHTFLsfc  0 122,1,0  ** surface Sensible heat flux [W/m^2]
SPFHprs 7 51,100,0 ** (profile) Specific humidity [kg/kg]
TCDCclm  0 71,200,0 ** atmos column Total cloud cover [%]
TMPprs 7 11,100,0 ** (profile) Temp. [K]
TMP2m  0 11,105,2 ** 2 m above ground Temp. [K]
UGRDprs 7 33,100,0 ** (profile) u wind [m/s]
UGRD10m  0 33,105,10 ** 10 m above ground u wind [m/s]
ULWRFsfc  0 212,1,0  ** surface Upward long wave flux [W/m^2]
USWRFsfc  0 211,1,0  ** surface Upward short wave flux [W/m^2]
VGRDprs 7 34,100,0 ** (profile) v wind [m/s]
VGRD10m  0 34,105,10 ** 10 m above ground v wind [m/s]
VISsfc  0 20,1,0  ** surface Visibility [m]
ENDVARS
