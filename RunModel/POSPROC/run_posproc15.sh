#!/bin/bash
# ---------------------------------------------------- #
# Script para o pos-processamento do WRF 
#
# Ronaldo Palmeira & Marcelo Romero - jan/2018
# ---------------------------------------------------- #

ANO=$1
MES=$2
DIA=$3
HOR=$4
FCST=$5
FCST2=$6

FCST=`echo $FCST + 1 | bc`

ANO1=`date +%Y --d "$ANO$MES$DIA $HOR +$FCST hours"`
MES1=`date +%m --d "$ANO$MES$DIA $HOR +$FCST hours"`
DIA1=`date +%d --d "$ANO$MES$DIA $HOR +$FCST hours"`
HOR1=`date +%H --d "$ANO$MES$DIA $HOR +$FCST hours"`
MESB=`LANG=US; date +%b --d "$ANO1$MES1$DIA1 $HOR1 +0 hours"`

ANOF=`date +%Y --d "$ANO$MES$DIA $HOR +$FCST2 hours"`
MESF=`date +%m --d "$ANO$MES$DIA $HOR +$FCST2 hours"`
DIAF=`date +%d --d "$ANO$MES$DIA $HOR +$FCST2 hours"`
HORF=`date +%H --d "$ANO$MES$DIA $HOR +$FCST2 hours"`

inFormat="netcdf"
tag="NCAR"


WRFPATH="/home/atmosmarine/PNT/WRFV3/"
UNIPOST="$WRFPATH/UPPV3.2/bin"
MPICH="/home/atmosmarine/tools/local/mpich/bin/"
GRADS="/home/atmosmarine/tools/local/opengrads/"
export PATH=$GRADS:$PATH

MDIR="/home/atmosmarine/PNT/RunModel/POSPROC"
cd $MDIR

# Linka o namelist do UPP (wrf_cntrl.parm)
ln -sf wrf_cntrl.parm fort.14


# Link microphysic's tables - code will use based on mp_physics option
# found in data
ln -fs ${WRFPATH}/test/em_real/ETAMPNEW_DATA nam_micro_lookup.dat
ln -fs ${WRFPATH}/test/em_real/ETAMPNEW_DATA.expanded_rain hires_micro_lookup.dat

# link coefficients for crtm2 (simulated synthetic satellites)
CRTMDIR=${WRFPATH}/UPPV3.1.1/src/lib/crtm2/src/fix
ln -fs $CRTMDIR/EmisCoeff/Big_Endian/EmisCoeff.bin           ./
ln -fs $CRTMDIR/AerosolCoeff/Big_Endian/AerosolCoeff.bin     ./
ln -fs $CRTMDIR/CloudCoeff/Big_Endian/CloudCoeff.bin         ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/imgr_g11.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/imgr_g11.TauCoeff.bin    ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/imgr_g12.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/imgr_g12.TauCoeff.bin    ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/imgr_g13.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/imgr_g13.TauCoeff.bin    ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/imgr_g15.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/imgr_g15.TauCoeff.bin    ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/imgr_mt1r.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/imgr_mt1r.TauCoeff.bin
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/imgr_mt2.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/imgr_mt2.TauCoeff.bin
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/imgr_insat3d.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/imgr_insat3d.TauCoeff.bin
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/amsre_aqua.SpcCoeff.bin  ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/amsre_aqua.TauCoeff.bin  ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/tmi_trmm.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/tmi_trmm.TauCoeff.bin    ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/ssmi_f13.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/ssmi_f13.TauCoeff.bin    ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/ssmi_f14.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/ssmi_f14.TauCoeff.bin    ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/ssmi_f15.SpcCoeff.bin    ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/ssmi_f15.TauCoeff.bin    ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/ssmis_f16.SpcCoeff.bin   ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/ssmis_f16.TauCoeff.bin   ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/ssmis_f17.SpcCoeff.bin   ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/ssmis_f17.TauCoeff.bin   ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/ssmis_f18.SpcCoeff.bin   ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/ssmis_f18.TauCoeff.bin   ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/ssmis_f19.SpcCoeff.bin   ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/ssmis_f19.TauCoeff.bin   ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/ssmis_f20.SpcCoeff.bin   ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/ssmis_f20.TauCoeff.bin   ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/seviri_m10.SpcCoeff.bin   ./
ln -fs $CRTMDIR/TauCoeff/ODPS/Big_Endian/seviri_m10.TauCoeff.bin   ./
ln -fs $CRTMDIR/SpcCoeff/Big_Endian/v.seviri_m10.SpcCoeff.bin   ./


# Roda o unipost para dada horario e para cada dominio

for GRD in d01; do  # 1 grade somente

rm -f WRFPRS.GrbF*

F=$FCST
while [ $F -lt $FCST2 ]; do 

ANO2=`date +%Y -d "$ANO$MES$DIA $HOR +$F hours"`
MES2=`date +%m -d "$ANO$MES$DIA $HOR +$F hours"`
DIA2=`date +%d -d "$ANO$MES$DIA $HOR +$F hours"`
HOR2=`date +%H -d "$ANO$MES$DIA $HOR +$F hours"`
inFileName="../RUN/wrfout_${GRD}_${ANO1}-${MES1}-${DIA1}_${HOR1}:00:00"

echo ${ANO2}-${MES2}-${DIA2}-${HOR2} $GRD

cat > itag <<EOF
${inFileName}
${inFormat}
${ANO2}-${MES2}-${DIA2}_${HOR2}:00:00
${tag}
EOF
$UNIPOST/unipost.exe > unipost.log

F=`expr $F + 1`

done # Fim do loop no FCST

# Converte os GRIBS em NetCDF novamente.
cat WRFPRS.GrbF??? > WRFPRS.15dias
$GRADS/grib2ctl.pl WRFPRS.15dias > wrf_tmp.ctl
ttime=`expr ${FCST2} - ${FCST}`
sed -i "/dset/c\dset ^WRFPRS.GrbF%f2" wrf_tmp.ctl
sed -i "/tdef/c\tdef ${ttime} linear ${HOR1}Z${DIA1}${MESB}${ANO1} 1hr" wrf_tmp.ctl
sed -i "/dtype grib 255/a options template" wrf_tmp.ctl

$GRADS/gribmap -i wrf_tmp.ctl

$GRADS/lats4d.sh -i wrf_tmp.ctl -o wrf_${GRD}_${ANO1}${MES1}${DIA1}${HOR1}.nc

$MDIR/calcula_agromet.py ${ANO1} ${MES1} ${DIA1} ${HOR1} ${GRD}

$MDIR/calcula_prevmet.py ${ANO1} ${MES1} ${DIA1} ${HOR1} ${GRD}

done # Fim do loop nos dominios

# Remove arquivos de output com mais de 5 dias
find wrf_d*.nc -ctime +5 -type f | xargs rm -rf
