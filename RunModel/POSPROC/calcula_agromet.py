#!/home/atmosmarine/tools/anaconda3/bin/python
# -*- coding: utf-8 -*-
#"""
#Calcula novas variaveis para fins AgroMEteorologicos 
#Created on Thu Jan 25 10:02:31 2018
#
#@author: ronaldo.palmeira, marcelo.romero
#"""

import matplotlib
matplotlib.use("Agg")
import xarray as xr
import pandas as pd
import numpy as np
import pyeto
import sys

ANO=sys.argv[1]
MES=sys.argv[2]
DIA=sys.argv[3]
HOR=sys.argv[4]
GRD=sys.argv[5] #d01"

nc = xr.open_dataset('wrf_{}_{}{}{}{}.nc'.format(GRD,ANO,MES,DIA,HOR) ) 

nc_out = xr.Dataset()
if HOR == "00":
   time_daily = pd.date_range('{}-{}-{} {}:00'.format(ANO,MES,DIA,HOR), freq='1D', periods=round((len(nc.time))/24))
else:
   time_daily = pd.date_range('{}-{}-{} {}:00'.format(ANO,MES,DIA,HOR), freq='1D', periods=round((len(nc.time)+24)/24))

tmean = nc.tmp2m.resample('D', dim='time', how='mean')-273.15
tmax = nc.tmp2m.resample('D', dim='time', how='max')-273.15
tmin = nc.tmp2m.resample('D', dim='time', how='min')-273.17
urmean = nc.rh2m.resample('D', dim='time', how='mean')
dptmean = nc.dpt2m.resample('D', dim='time', how='mean')-273.15
presmean = nc.pressfc.resample('D', dim='time', how='mean')/1000.0

vvel10m = np.sqrt(nc.ugrd10m**2+nc.vgrd10m**2)
vvel10m = vvel10m.resample('D', dim='time', how='mean')

accshflx = nc.shtflsfc.resample('D', dim='time', how='mean')*0.0864 # 1 W m-2 = 0.0864 MJ m-2 day-1
acclhflx = nc.lhtflsfc.resample('D', dim='time', how='mean')*0.0864 # 1 W m-2 = 0.0864 MJ m-2 day-1
accghflx = nc.gfluxsfc.resample('D', dim='time', how='mean')*0.0864 # 1 W m-2 = 0.0864 MJ m-2 day-1

nc.dlwrfsfc[0,:,:] = nc.dlwrfsfc[0,:,:]*0
nc.dswrfsfc[0,:,:] = nc.dswrfsfc[0,:,:]*0

outl_rad = nc.ulwrfsfc.resample('D', dim='time', how='mean')*0.0864
outs_rad = nc.uswrfsfc.resample('D', dim='time', how='mean')*0.0864
incl_rad = nc.dlwrfsfc.resample('D', dim='time', how='mean')*0.0864
incs_rad = nc.dswrfsfc.resample('D', dim='time', how='mean')*0.0864

et_rad = nc.dswrftoa.resample('D', dim='time', how='mean')*0.0864


# -- Conforto termico animal (ITU) --
# Ta: temp media do ar (acho que diaria)
# UR: umidade relativa media do ar (acho que diaria)
ITU = 0.8 * tmean + urmean * (tmean - 14.3)/100 + 46.3

nc_out['ITU'] = xr.DataArray(ITU, dims=('time','latitude','longitude'), 
                         coords={'time': time_daily, 'latitude': nc.latitude, 
                                 'longitude':nc.longitude})
nc_out.ITU.attrs['long_name'] = "Conforto termico animal"


# -- Graus dia (GD) --
# tb=> temperatura basal (dado) p/ tb => 10,12,14 e 16C
GD1 = (tmax-tmin)/2 - 10.0
nc_out['GD1'] = xr.DataArray(GD1, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.GD1.attrs['long_name'] = "Graus Dia com temp basal 10C"

GD2 = (tmax-tmin)/2 - 12.0
nc_out['GD2'] = xr.DataArray(GD2, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.GD2.attrs['long_name'] = "Graus Dia com temp basal 12C"

GD3 = (tmax-tmin)/2 - 14.0
nc_out['GD3'] = xr.DataArray(GD3, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.GD3.attrs['long_name'] = "Graus Dia com temp basal 14C"

GD4 = (tmax-tmin)/2 - 16.0
nc_out['GD4'] = xr.DataArray(GD4, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.GD4.attrs['long_name'] = "Graus Dia com temp basal 16C"


# --  Horas de frio --
# NHF<7,0 °C (Guidoni et. al., 1982)
# Numero de horas do dia em questao onde a temperatura (T)
# do WRF fico abaixo de 7,0 °C
temp = nc.tmp2m-273.15
temp = temp.where(temp<7)  # Coloquei 15C para teste!!!!
temp = temp/temp
NHF = temp.resample('D', dim='time', how='sum')
nc_out['NHF'] = xr.DataArray(NHF, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.NHF.attrs['long_name'] = "Numero de horas de frio (< 7C)"
nc_out.NHF.attrs['units'] = "hours"


# -- Duracao do moliamento foliar (Sentelhas e Gillespie 1982) --
# Sentelhas (NHUR) 
# NHRH > 90%: RH = 90% was considered as limit
# for the beginning of dew deposition (Sentelhas, 2004).
# The number of intervals of 20 min (during one day)
# with RH above 90% divided by three was considered
# as LWD (h).
# Numero de horas com UR superior a 87%
ur = nc.rh2m.where(nc.rh2m>87) 
ur = ur/ur
NHUR = ur.resample('D', dim='time', how='sum')

nc_out['NHUR'] = xr.DataArray(NHUR, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.NHUR.attrs['long_name'] = "Molhamento foliar (Sentelhas,1982)"
nc_out.NHUR.attrs['units'] = "hours"

# Gillespie (DPO)
# DPD: the difference between T and the dew point temperature
# (To) was suggested as a LWD estimate
# method by Gillespie et al. (1993), being the time interval
# when DPD stays below two specific limits: 2.0ºC
# for the dew onset and 3.8ºC for the dew dry-off (Rao
# et al., 1998)
# Numero de horas com Dew Point entre os valores abaixo:
# 2,0 °C < DPO <3,8 °C (Gillespie et. al., 1993); 
td = nc.dpt2m-273.15
td1 = td.where( td > 2.0 )
td2 = td.where( td < 3.8 )
td3 = (td1+td2)/(td1+td2)
DPO = td3.resample('D', dim='time', how='sum')

nc_out['DPO'] = xr.DataArray(DPO, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.DPO.attrs['long_name'] = "Molhamento foliar (Gillespie,1998)"
nc_out.DPO.attrs['units'] = "hours"


# -- Risco de Incendio (RI) --
# T: temperarura do ar
# UR: umidade relativa do ar
# Variavaveis acima medidas às 13 (16 UTC)
time_fire = pd.date_range('{}-{}-{} 16:00'.format(ANO,MES,DIA), freq='1D', periods=7)

tmp16h = nc.tmp2m.sel(time=time_fire)
rh16h = nc.rh2m.sel(time=time_fire)

RI = 0.05 * rh16h - 0.1 * (tmp16h - 27.0)
RI = RI.values

if HOR=="12": 
    time_fire = time_daily[0:-1]
else:
    time_fire = time_daily

#nc_out['RI'] = xr.DataArray(RI, dims=('time','latitude','longitude'),
#                         coords={'time': time_fire, 'latitude': nc.latitude,
#                                 'longitude':nc.longitude})
#nc_out.RI.attrs['long_name'] = "Risco de Incendio"


# -- ETo estimation from FAO-56 Penman-Monteith equation --
# Using Marcelo's script

Rn = (accshflx + acclhflx + accghflx)
G = accghflx
U2=vvel10m*(4.87/(np.log(67.8*10-5.42 )))

gama=0.665*presmean*10**(-3)				# P tem no WRF

es=0.6108*np.exp( (17.27*tmean)/(tmean+237.3) )
ea=es*(urmean/100)				# UR tem no WRF

delta= (4098*es)/((tmean+237.3)**2)

termo4=0.408*delta*(Rn-G) + gama*(900/(tmean+273))*U2*(es-ea)
termo5=delta+gama*(1+0.34*U2)

ET0=termo4/termo5


nc_out['ET0'] = xr.DataArray(ET0, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.ET0.attrs['long_name'] = "ETo estimation from FAO-56 Penman-Monteith"
nc_out.ET0.attrs['units'] = "mm"

# -- Estimate reference evapotranspiration over grass (ETo) 
#  Using Hargreaves equation based on equation 52 in Allen et al (1998).
ETH = 0.0023 * (tmean + 17.8) * (tmax - tmin) ** 0.5 * 0.408 * et_rad
nc_out['ETH'] = xr.DataArray(ETH, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.ETH.attrs['long_name'] = "ETo estimation from Hargreaves"
nc_out.ETH.attrs['units'] = "mm"


nc_out.to_netcdf('wrf_{}_{}{}{}{}_agromet.nc'.format(GRD,ANO,MES,DIA,HOR) )
