#!/home/atmosmarine/tools/anaconda3/bin/python
# -*- coding: utf-8 -*-
#"""
# Calcula as variaveis de previsao 4xdia
# 
# 03-08 (06z) Madrugada
# 09-14 (12z) Manha
# 15-20 (18z) Tarde
# 21-02 (06z) Noite
#
#Created on Thu Jan 25 10:02:31 2018
#
#@author: ronaldo.palmeira, marcelo.romero
#"""

import matplotlib
matplotlib.use("Agg")
import xarray as xr
import pandas as pd
import numpy as np
import pyeto
import sys

ANO=sys.argv[1]
MES=sys.argv[2]
DIA=sys.argv[3]
HOR=sys.argv[4]
GRD=sys.argv[5] #d01"

nc = xr.open_dataset('wrf_{}_{}{}{}{}.nc'.format(GRD,ANO,MES,DIA,HOR) ) 

nc_out = xr.Dataset()

if round((len(nc.time))) >= 169 :
    time_daily = pd.date_range('{}-{}-{} {}:00'.format(ANO,MES,DIA,HOR), freq='6H', periods=33)
else:
    time_daily = pd.date_range('{}-{}-{} {}:00'.format(ANO,MES,DIA,HOR), freq='6H', periods=29)


# -- Temperatura Media, Minima e Maxima --
tmed = nc.tmp2m.resample('6H', dim='time', base=3, how='mean')-273.15
nc_out['tmed'] = xr.DataArray(tmed, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.tmed.attrs['long_name'] = "Temperatura media do ar a 2m"
nc_out.tmed.attrs['units'] = "C"

tmin = nc.tmp2m.resample('6H', dim='time', base=3, how='min')-273.17
nc_out['tmin'] = xr.DataArray(tmin, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.tmin.attrs['long_name'] = "Temperatura minima do ar a 2m"
nc_out.tmin.attrs['units'] = "C"

tmax = nc.tmp2m.resample('6H', dim='time', base=3, how='max')-273.15
nc_out['tmax'] = xr.DataArray(tmax, dims=('time','latitude','longitude'), 
                         coords={'time': time_daily, 'latitude': nc.latitude, 
                                 'longitude':nc.longitude})
nc_out.tmax.attrs['long_name'] = "Temperatura maxima do ar a 2m"
nc_out.tmax.attrs['units'] = "C"


# -- Umidade Media, Minima e Maxima --
urmed = nc.rh2m.resample('6H', dim='time', base=3, how='mean')
nc_out['urmed'] = xr.DataArray(urmed, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.urmed.attrs['long_name'] = "Umidade media do ar a 2m"
nc_out.urmed.attrs['units'] = "%"

urmin = nc.rh2m.resample('6H', dim='time', base=3, how='min')
nc_out['urmin'] = xr.DataArray(urmin, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.urmin.attrs['long_name'] = "Umidade minima do ar a 2m"
nc_out.urmin.attrs['units'] = "%"

urmax = nc.rh2m.resample('6H', dim='time', base=3, how='max')
nc_out['urmax'] = xr.DataArray(urmax, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.urmax.attrs['long_name'] = "Umidade maxima do ar a 2m"
nc_out.urmax.attrs['units'] = "%"

# -- Pressão reduzida a superficie do mar --
presmean = nc.prmslmsl.resample('6H', dim='time', base=3, how='mean')/1000.0
nc_out['presmean'] = xr.DataArray(presmean, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.presmean.attrs['long_name'] = "Pressao media ao nivel do mar"
nc_out.presmean.attrs['units'] = "hPa"

# -- Velocicade e direção do vento --
vvel10m = np.sqrt(nc.ugrd10m**2+nc.vgrd10m**2)
vvel10max= vvel10m.resample('6H', dim='time', base=3, how='max')
nc_out['vvel10max'] = xr.DataArray(vvel10max, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.vvel10max.attrs['long_name'] = "Velocidade maxima do vento a 10m"
nc_out.vvel10max.attrs['units'] = "m/s"

vvel10mean = vvel10m.resample('6H', dim='time', base=3, how='mean')
nc_out['vvel10mean'] = xr.DataArray(vvel10mean, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.vvel10mean.attrs['long_name'] = "Velocidade media do vento a 10m"
nc_out.vvel10mean.attrs['units'] = "m/s"

gust10m = nc.gustsfc.resample('6H', dim='time', base=3, how='max')
nc_out['gust10m'] = xr.DataArray(gust10m, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.gust10m.attrs['long_name'] = "Velocidade media do vento a 10m"
nc_out.gust10m.attrs['units'] = "m/s"

#vdir10m = np.atan2(-nc.ugrd10m,-nc.vgrd10m)
#vdir10m = vdir10m.resample('6H', dim='time', base=3, how=mode)
#nc_out['vdir10m'] = xr.DataArray(vdir10m, dims=('time','latitude','longitude'),
#                         coords={'time': time_daily, 'latitude': nc.latitude,
#                                 'longitude':nc.longitude})
#nc_out.vdir10m.attrs['long_name'] = "Direcao predominante do vento a 10m"
#nc_out.vdir10m.attrs['units'] = "degress"

# -- Precipitacao acumulada --
prec = nc.apcpsfc.resample('6H', dim='time', base=3, how='last')
prec.values[prec.values<=0.2] = 0.0
prec = xr.concat([prec[0,:,:], prec.diff('time')],dim='time').transpose('time','latitude','longitude')
nc_out['prec'] = xr.DataArray(prec, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.prec.attrs['long_name'] = "Precipitacao acumulada"
nc_out.prec.attrs['units'] = "mm"

# -- Media de Nebulosidade integrada --
cloud = nc.tcdcclm.resample('6H', dim='time', base=3, how='mean')
nc_out['cloud'] = xr.DataArray(cloud, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.cloud.attrs['long_name'] = "Media de Nebulosidade integrada"
nc_out.cloud.attrs['units'] = "%"

# -- Media de Nebulosidade baixa --
cloudl = nc.lcdclcl.resample('6H', dim='time', base=3, how='mean')
nc_out['cloudl'] = xr.DataArray(cloudl, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.cloudl.attrs['long_name'] = "Media de Nebulosidade baixa"
nc_out.cloudl.attrs['units'] = "%"

# -- Media de Nebulosidade media  --
cloudm = nc.mcdcmcl.resample('6H', dim='time', base=3, how='mean')
nc_out['cloudm'] = xr.DataArray(cloudm, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.cloudm.attrs['long_name'] = "Media de Nebulosidade em niveis medios"
nc_out.cloudm.attrs['units'] = "%"

# -- Media de Nebulosidade alta --
cloudh = nc.hcdchcl.resample('6H', dim='time', base=3, how='mean')
nc_out['cloudh'] = xr.DataArray(cloudh, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.cloudh.attrs['long_name'] = "Media de Nebulosidade alta"
nc_out.cloudh.attrs['units'] = "%"


# -- Calcula icone --
"""
if nuvem>=20  :prev=u'Poucas nuvens' ; icone='3.png'
   if nuvem>=40  :prev=u'Parc. Nublado' ; icone='4.png'
   if nuvem>=60  :prev=u'Céu Nublado' ; icone='4x.png'
   if nuvem>=80  :prev=u'Céu Encoberto' ; icone='7.png'
   if nuvem<20  :prev=u'Céu claro'  ; icone='2.png'
   if chuva>0.4 and chuva<10 and nuvem<80 :prev=u'Chuva rápida'; icone='6.png'
   if chuva>=10 and nuvem<80 :prev=u'Pancadas de chuva'; icone='5x.png'
   if chuva<=0.2: chuva=0.0
   if chuva>0.4 and chuva<= 1.0 and nuvem>=80: prev=u'Possível chuvisco'    ; icone='7x.png'
   if chuva>1 and chuva<= 5.0 and nuvem>=80: prev=u'Chuva leve'    ; icone='8n.png'
   if chuva>5.0 and nuvem>=80: prev=u'Chuva moderada' ; icone='8.png'
   if chuva>25 and nuvem>=80 :prev=u'Chuva forte'    ; icone='8x.png'
if p>=2: #Noite
   if nuvem>=20  :prev=u'Poucas nuvens' ; icone='3p.png'
   if nuvem>=40  :prev=u'Parc. Nublado' ; icone='4p.png'
   if nuvem>=60  :prev=u'Céu Nublado' ; icone='4xp.png'
   if nuvem>=80  :prev=u'Céu Encoberto' ; icone='7.png'
   if nuvem<20  :prev=u'Céu claro'  ; icone='2p.png'
   if chuva>0.4 and chuva<10 and nuvem<80 :prev=u'Chuva rápida'; icone='6p.png'
   if chuva>=10 and nuvem<80 :prev=u'Pancadas de chuva'; icone='5xp.png'
   if chuva<=0.2: chuva=0.0
   if chuva>0.4 and chuva<= 1.0 and nuvem>=80: prev=u'Possível chuvisco'    ; icone='7x.png'
   if chuva>0.2 and chuva<= 5.0 and nuvem>=80: prev=u'Chuva leve'    ; icone='8n.png'
   if chuva>5.0 and nuvem>=80: prev=u'Chuva moderada' ; icone='8.png'
   if chuva>25 and nuvem>=80:prev=u'Chuva forte'    ; icone='8x.png'
"""
cloud = (cloudl+cloudm)/2
prev=cloud/cloud
prev.values[cloud.values<20] = 2.0
prev.values[cloud.values>=20] = 3.0
prev.values[cloud.values>=40] = 4.0
prev.values[cloud.values>=60] = 4.5 # 4x
prev.values[(prec.values>=10.0) & (cloud.values<80)] = 5.5 # 5x
prev.values[(prec.values>0.4) & (prec.values<10) & (cloud.values<80)] = 6.0
prev.values[cloud.values>=80] = 7.0
prev.values[(prec.values>0.4) & (prec.values<= 1.0) & (cloud.values>=80)] = 7.5   # 7x
prev.values[(prec.values>0.2) & (prec.values<= 5.0) & (cloud.values>=80)] = 8.2   # 8n
prev.values[(prec.values>5.0) & (cloud.values>=80)] = 8.6   # 8
prev.values[(prec.values>25.0) & (cloud.values>=80)] = 8.8   # 8x

nc_out['prev'] = xr.DataArray(prev, dims=('time','latitude','longitude'),
                         coords={'time': time_daily, 'latitude': nc.latitude,
                                 'longitude':nc.longitude})
nc_out.prev.attrs['long_name'] = "Codigo de previsao"



nc_out.to_netcdf('wrf_{}_{}{}{}{}_prevmet.nc'.format(GRD,ANO,MES,DIA,HOR) )
