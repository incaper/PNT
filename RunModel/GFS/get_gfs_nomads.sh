#!/bin/bash
##################################################################
#  Script para download dos dados 3D GFS 0.5g via NOMADS p/ WRF  #  
#  Marcelo Romero e Ronaldo Palmeira                             #
#  Out/2017                                                      #
##################################################################
export ftp_proxy="http://10.183.254.68:3128"
export http_proxy="http://10.183.254.68:3128"
export https_proxy="http://10.183.254.68:3128"

# Recebe as datas
ANO=$1  #`date +'%Y'`
MES=$2  #`date +'%m'`
DIA=$3  #`date +'%d'`
HOR=$4  #"00" #$1
FCST=$5


DIR=/home/atmosmarine/PNT/RunModel/GFS 
cd $DIR
mkdir -p $DIR/gfs.$ANO$MES$DIA$HOR
MDIR=$DIR/gfs.$ANO$MES$DIA$HOR

F="000"
while [ $F -le $FCST ]; do
echo $F

wget -c http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${HOR}z.pgrb2.0p25.f${F}'&'all_lev=on'&'all_var=on'&'subregion=on'&'leftlon=270'&'rightlon=360'&'toplat=10'&'bottomlat=-60'&'dir=%2Fgfs.$ANO$MES$DIA$HOR -O $MDIR/gfs.${ANO}${MES}${DIA}${HOR}_${F}.grb2

F=`expr $F + 6`
F=$(printf "%03d" $F)


done
