#!/bin/sh
##################################################################
#  Script para download dos dados 3D GFS 0.5g via NOMADS p/ WRF  #  
#  Marcelo Romero e Ronaldo Palmeira                             #
#  Out/2017                                                      #
##################################################################
source $HOME/.profile

export ftp_proxy="http://10.183.254.68:3128"
export http_proxy="http://10.183.254.68:3128"
export https_proxy="http://10.183.254.68:3128"

DIR=/home/atmosmarine/PNT/GFS       # COLOCAR O CAMINHO ONDE VC VAO FICAR OS DADOS

# ---- BAIXANDO ARQUIVOS DE 1 a 8 DIAS ---- # 
cd $DIR
mkdir -p $DIR/gfs_nomads.$ANO$MES$DIA$HORA
cd $DIR/gfs_nomads.$ANO$MES$DIA$HORA

aa=`date +'%Y'`
yy=`date +'%y'`
mm=`date +'%m'`
dd=`date +'%d'`
tim=`date +'%T'`
sdat=$aa$mm$dd
sdat=$sdat'00'


for fhr in 000 006 012 018 024 030 036 042 048 054 060 066 072 078 084 090 096 102 108 114 120 126 132 138 144 150 156 162 168 174 180 186 192 
do
echo $fhr
URL="http://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25_1hr.pl?\
file=gfs.t00z.pgrb2.0p25.f${fhr}&\
all_lev=on&\
all_var=on&\
subregion=&\
leftlon=-54.0&\
rightlon=-27.0&\
toplat=-14.0&\
bottomlat=-32.0&\
dir=%2Fgfs.${sdat}"


# download file
curl "$URL" -o download${fhr}.grb
# add a sleep to prevent a denial of service in case of missing file
sleep 1
mv download${fhr}.grb gfs.t00z.pgrb2.0p25.f${fhr}
done

exit

