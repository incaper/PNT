#!/bin/bash
################################################################
#  Script para download dos dados 3D GFS 0.5g via CURL p/ WRF  #  
#  Ronaldo Palmeira - palmeira@gmail.com                       #
#  Jan/2009                                                    #
################################################################
source $HOME/.profile 

export ftp_proxy="http://10.183.254.68:3128"
export http_proxy="http://10.183.254.68:3128"
export https_proxy="http://10.183.254.68:3128"

DIR=/home/atmosmarine/PNT/GFS       # COLOCAR O CAMINHO ONDE VC VAO FICAR OS DADOS

ANO=`date +%Y` 
MES=`date +%m`  
DIA=`date +%d`  
HORA=${1}
MESB=`LANG=US date +"%b" --date=""$MES"/"$DIA"/"$ANO""`
res=25       # 50 # Resolucao do GFS (25->0.25graus)
SERVER=http://www.ftp.ncep.noaa.gov
#SERVER=ftp://ftp.ncep.noaa.gov/pub


# --- SELECIONANDO APENAS OS CAMPOS E NIVEIS NECESSARIOS PARA DOWNLOAD --- #
RH=":RH:1000 mb:|:RH:975 mb:|:RH:950 mb:|:RH:925 mb:|:RH:900 mb:|:RH:850 mb:|:RH:800 mb:|:RH:750 mb:|:RH:700 mb:|:RH:650 mb:|:RH:600 mb:|:RH:550 mb:|:RH:500 mb:|:RH:450 mb:|:RH:400 mb:|:RH:350 mb:|:RH:300 mb:|:RH:250 mb:|:RH:200 mb:|:RH:150 mb:|:RH:100 mb:|:RH:50 mb:|:RH:2 m above ground:"
UGRD=":UGRD:1000 mb:|:UGRD:975 mb:|:UGRD:950 mb:|:UGRD:925 mb:|:UGRD:900 mb:|:UGRD:850 mb:|:UGRD:800 mb:|:UGRD:750 mb:|:UGRD:700 mb:|:UGRD:650 mb:|:UGRD:600 mb:|:UGRD:550 mb:|:UGRD:500 mb:|:UGRD:450 mb:|:UGRD:400 mb:|:UGRD:350 mb:|:UGRD:300 mb:|:UGRD:250 mb:|:UGRD:200 mb:|:UGRD:150 mb:|:UGRD:100 mb:|:UGRD:50 mb:|:UGRD:10 m above ground:"
VGRD=":VGRD:1000 mb:|:VGRD:975 mb:|:VGRD:950 mb:|:VGRD:925 mb:|:VGRD:900 mb:|:VGRD:850 mb:|:VGRD:800 mb:|:VGRD:750 mb:|:VGRD:700 mb:|:VGRD:650 mb:|:VGRD:600 mb:|:VGRD:550 mb:|:VGRD:500 mb:|:VGRD:450 mb:|:VGRD:400 mb:|:VGRD:350 mb:|:VGRD:300 mb:|:VGRD:250 mb:|:VGRD:200 mb:|:VGRD:150 mb:|:VGRD:100 mb:|:VGRD:50 mb:|:VGRD:10 m above ground:"
TMP=":TMP:1000 mb:|:TMP:975 mb:|:TMP:950 mb:|:TMP:925 mb:|:TMP:900 mb:|:TMP:850 mb:|:TMP:800 mb:|:TMP:750 mb:|:TMP:700 mb:|:TMP:650 mb:|:TMP:600 mb:|:TMP:550 mb:|:TMP:500 mb:|:TMP:450 mb:|:TMP:400 mb:|:TMP:350 mb:|:TMP:300 mb:|:TMP:250 mb:|:TMP:200 mb:|:TMP:150 mb:|:TMP:100 mb:|:TMP:50 mb:|:TMP:2 m above ground:|:TMP:surface:"
HGT=":HGT:1000 mb:|:HGT:975 mb:|:HGT:950 mb:|:HGT:925 mb:|:HGT:900 mb:|:HGT:850 mb:|:HGT:800 mb:|:HGT:750 mb:|:HGT:700 mb:|:HGT:650 mb:|:HGT:600 mb:|:HGT:550 mb:|:HGT:500 mb:|:HGT:450 mb:|:HGT:400 mb:|:HGT:350 mb:|:HGT:300 mb:|:HGT:250 mb:|:HGT:200 mb:|:HGT:150 mb:|:HGT:100 mb:|:HGT:50 mb:|:HGT:surface:"
SOILT=":TSOIL:0-0.1 m below ground:|:TSOIL:0.1-0.4 m below ground:|:TSOIL:0.4-1 m below ground:|:TSOIL:1-2 m below ground:"
SOILW=":SOILW:0-0.1 m below ground:|:SOILW:0.1-0.4 m below ground:|:SOILW:0.4-1 m below ground:|:SOILW:1-2 m below ground:"
VARSGET="$HGT|$TMP|$UGRD|$VGRD|$RH|PRES:surface|$SOILT|$SOILW|WEASD|ICEC|CICEP|PRMSL|LAND"


# ---- BAIXANDO ARQUIVOS DE 1 a 8 DIAS ---- # 
cd $DIR
mkdir -p $DIR/gfs.$ANO$MES$DIA$HORA
horas='000 012 024 036 048 060 072 084 096 108 120 132 144 156 168 180 192'
for h in $horas;do
TAM=0
TRIES=1
while [ $TAM -lt 30000000 ] && [ $TRIES -le 50 ]; do
echo ">>>> Transferindo previsao GFS(AVN) de $ANO$MES$DIA para $h horas..."
# --- CURL VIA WWW.FTP.NCEP.NOAA.GOV --- #
if [ "$TAM" -lt 1000 ]; then
    echo ">>>> fonte 1: $SERVER - tentativa: $TRIES"
    $DIR/get_inv.pl $SERVER/data/nccf/com/gfs/prod/gfs.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2.0p$res.f$h.idx | egrep "($VARSGET)" | $DIR/get_grib.pl $SERVER/data/nccf/com/gfs/prod/gfs.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2.0p$res.f$h $DIR/gfs.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2f$h
    test -f $DIR/gfs.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2f$h 
    TE=$?
    if [ "$TE" -eq 1 ]; then
      TAM=0
    else
      TAM=`du -sb $DIR/gfs.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2f$h | awk '{ print $1 }'`
    fi
fi

TRIES=`expr $TRIES + 1`
done
done

# --- Limpa NCs com mais de 5 dias.
cd $DIR
find gfs.?????????? -ctime +4 -type d | xargs rm -rf
