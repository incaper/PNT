#!/bin/bash
#################################################################################
#  Script para download dos dados SFC GFS 0.5g ou 0.25g via CURL p/ WW3 e HYCOM #  
#  Ronaldo Palmeira - palmeira@gmail.com                                        #
#  Desenvolvido em Jan/2009                                                     #
#  Modificado em mar/2015                                                       #
#################################################################################
DIR=/home/ronaldo/RunModels/GFS_NCEP       # COLOCAR O CAMINHO ONDE VAO FICAR OS DADOS

ANO=`date +%Y` 
MES=`date +%m`  
MESB=`LANG=US; date +%b`  
DIA=`date +%d`  
HORA=${1} 
MESB=`LANG=US date +"%b" --date=""$MES"/"$DIA"/"$ANO""`
res=25 # 50 # Resolucao do GFS (25->0.25graus)
SERVER=http://www.ftp.ncep.noaa.gov
#SERVER=ftp://ftp.ncep.noaa.gov/pub


# --- SELECIONANDO APENAS OS CAMPOS EM SUPERFICIE (UTILIZADOS NO PREPROC DO WW3, ROMS, DELFT e HYCOM ) --- #
RH=":RH:2 m above ground:"
SPFH=":SPFH:2 m above ground:"
RAD=":DSWRF:surface|:USWRF:surface:|:DLWRF:surface:|:ULWRF:surface:"
PREC=":APCP:surface:"
UGRD=":UGRD:10 m above ground:"
VGRD=":VGRD:10 m above ground:"
TMP=":TMP:2 m above ground:|:TMP:surface:"
PREC=":ACPCP:surface:|PRATE:surface:"
GUST=":GUST:surface:"
CLOUD=":TCDC:low cloud layer:|:TCDC:middle cloud layer:|:TCDC:high cloud layer:"
VARSGET="$TMP|$UGRD|$VGRD|$RH|$SPFH|PRES:surface|WEASD|ICEC|CICEP|PRMSL|LAND|$PREC|$GUST|$CLOUD"

# ---- BAIXANDO ARQUIVOS DE 3 em 3 horas ate 7 DIAS ---- # 
cd $DIR
mkdir -p $DIR/gfs_sfc.$ANO$MES$DIA$HORA

h='000'
hfim='072'
dt=3
nt=$(echo "${hfim}/${dt} + 1" | bc)

while [ $h -le $hfim ]; do
horas=`echo $(printf %03d%s ${h})`
TAM=0; TRIES=1
while [ $TAM -lt 290000 ] && [ $TRIES -le 50 ]; do
echo ">>>> Transferindo previsao GFS(AVN) de $ANO$MES$DIA para $horas horas..."
# --- CURL VIA WWW.FTP.NCEP.NOAA.GOV --- #
if [ "$TAM" -lt 290000 ]; then
    echo ">>>> fonte 1: $SERVER - tentativa: $TRIES"
    echo "$DIR/get_inv.pl $SERVER/data/nccf/com/gfs/prod/gfs.$ANO$MES$DIA$HORA/gfs.t${HORA}z.pgrb2.0p$res.f$horas.idx"
    #$DIR/get_inv.pl $SERVER/data/nccf/com/gfs/prod/gfs.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2.0p$res.f$horas.idx | egrep "($VARSGET)" | $DIR/get_grib.pl $SERVER/data/nccf/com/gfs/prod/gfs.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2.0p$res.f$horas $DIR/gfs_sfc.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2f$horas
    test -f $DIR/gfs_sfc.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2f$horas
    TE=$?
    if [ "$TE" -eq 1 ]; then
      TAM=0
    else
      TAM=`du -sb $DIR/gfs_sfc.$ANO$MES$DIA$HORA/gfs.t"$HORA"z.pgrb2f$horas | awk '{ print $1 }'`
    fi
fi

TRIES=`expr $TRIES + 1`
done
h=`expr $horas + $dt`
done

cat > $DIR/gfs_sfc.$ANO$MES$DIA$HORA/gfs_sfc_7dias.ctl <<EOF
dset ^gfs.t00z.pgrb2f%f3
index ^gfs.t00z.pgrb2.idx
undef 9.999E+20
title GFS surface
options template
dtype grib2
ydef 721 linear -90.000000 0.25
xdef 1440 linear 0.000000 0.250000
tdef $nt linear ${HORA}Z${DIA}${MESB}${ANO} ${dt}hr
zdef 1 linear 1 1
vars 18
ACPCPsfc   0,1,0   0,1,10,1 ** surface Convective Precipitation [kg/m^2]
CICEPsfc   0,1,0   0,1,194,0 ** surface Categorical Ice Pellets (yes=1; no=0) [non-dim]
GUSTsfc   0,1,0   0,2,22 ** surface Wind Speed (Gust) [m/s]
ICECsfc   0,1,0   10,2,0 ** surface Ice Cover [Proportion]
LANDsfc   0,1,0   2,0,0 ** surface Land Cover (1=land, 0=sea) [Proportion]
PRATEsfc   0,1,0   0,1,7,0 ** surface Precipitation Rate [kg/m^2/s]
PRESsfc   0,1,0   0,3,0 ** surface Pressure [Pa]
PRMSLmsl   0,101,0   0,3,1 ** mean sea level Pressure Reduced to MSL [Pa]
RH2m   0,103,2   0,1,1 ** 2 m above ground Relative Humidity [%]
SPFH2m   0,103,2   0,1,0 ** 2 m above ground Specific Humidity [kg/kg]
TCDClcll   0,214,0   0,6,1,0 ** low cloud layer Total Cloud Cover [%]
TCDCmcll   0,224,0   0,6,1,0 ** middle cloud layer Total Cloud Cover [%]
TCDChcll   0,234,0   0,6,1,0 ** high cloud layer Total Cloud Cover [%]
TMPsfc   0,1,0   0,0,0 ** surface Temperature [K]
TMP2m   0,103,2   0,0,0 ** 2 m above ground Temperature [K]
UGRD10m   0,103,10   0,2,2 ** 10 m above ground U-Component of Wind [m/s]
VGRD10m   0,103,10   0,2,3 ** 10 m above ground V-Component of Wind [m/s]
WEASDsfc   0,1,0   0,1,13 ** surface Water Equivalent of Accumulated Snow Depth [kg/m^2]
ENDVARS
EOF

cd $DIR/gfs_sfc.$ANO$MES$DIA$HORA/
gribmap -i gfs_sfc_7dias.ctl
lats4d.sh -i gfs_sfc_7dias.ctl -o gfs_sfc_7d_$ANO$MES$DIA$HORA.nc0
ncks -4 --deflate=1 gfs_sfc_7d_$ANO$MES$DIA$HORA.nc0.nc gfs_sfc_7d_$ANO$MES$DIA$HORA.nc
#rm -f gfs_sfc_7d_$ANO$MES$DIA$HORA.nc0.nc

echo "Envia NCs pra Darth"
#ncftpput -u operacao -p OpERACAO01 10.50.12.21 /NCs gfs_sfc_7d_${ANO}${MES}${DIA}${HORA}.nc
#ncftpput -u operacao -p OpERACAO01 10.50.12.21 /NCs wind_gfs_7d_${ANO}${MES}${DIA}${HORA}.nc


# --- Limpa NCs com mais de 5 dias.
cd $DIR
find gfs_sfc.?????????? -ctime +4 -type d | xargs rm -rf
