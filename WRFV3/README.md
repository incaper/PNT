
![http://atmosmarine.com](https://tlgur.com/d/0Gomy7b4)

# Compilação do modelo WRF (versão 3.8.1):

A compilação do WRF foi realizada seguindo o tutorial do próprio NCAR disponibilizado em:

http://www2.mmm.ucar.edu/wrf/OnLineTutorial/compilation_tutorial.php

Os códigos foram obtidos através do link: http://www2.mmm.ucar.edu/wrf/users/downloads.html

* A base de dados geográficos não consta neste repositório por uma questão de espaço, portanto ela deve ser baixada através do link: http://www2.mmm.ucar.edu/wrf/users/download/get_sources_wps_geog.html e descompactada no diretorio WRFV3/WPS/geog/.

A seguir os passos de compilação de cada uma das bibliotecas necessárias para compilação do WRF.


**NETCDF:**
```
export DIR=path_to_directory/Build_WRF/LIBRARIES
export CC=gcc
export CXX=g++
export FC=gfortran
export FCFLAGS="-m64"
export F77=gfortran
export FFLAGS="-m64"

tar xzvf netcdf-4.1.3.tar.gz     #or just .tar if no .gz present
cd netcdf-4.1.3
./configure --prefix=$DIR/netcdf --disable-dap \
     --disable-netcdf-4 --disable-shared
make
make install
```

**MPICH:**
```
tar xzvf mpich-3.0.4.tar.gz     #or just .tar if no .gz present
cd mpich-3.0.4
./configure --prefix=$DIR/mpich
make
make install
setenv PATH $DIR/mpich/bin:$PATH
```

**ZLIB:**
```
setenv LDFLAGS -L$DIR/grib2/lib 
setenv CPPFLAGS -I$DIR/grib2/include 

tar xzvf zlib-1.2.7.tar.gz     #or just .tar if no .gz present
cd zlib-1.2.7
./configure --prefix=$DIR/grib2
make
make install
```

**LIBPNG:**
```
tar xzvf libpng-1.2.50.tar.gz     #or just .tar if no .gz present
cd libpng-1.2.50
./configure --prefix=$DIR/grib2
make
make install
```

**JASPER:**
```
tar xzvf jasper-1.900.1.tar.gz     #or just .tar if no .gz present
cd jasper-1.900.1
./configure --prefix=$DIR/grib2
make
make install
```


## Compilação WRF:


**WRF:**
```
export JASPERLIB=$DIR/grib2/lib
export JASPERINC=$DIR/grib2/include
export PATH=$DIR/netcdf/bin:$PATH
export NETCDF=$DIR/netcdf

gunzip WRFV3.8.1.TAR.gz
tar -xf WRFV3.8.1.TAR
cd WRFV3
./clean -a
./configure (opção Linux x86_64 ppc64le, gfortran compiler with gcc   (dmpar))
./compile wrf
./compile em_real
```
**WPS:**
```
gunzip WPSV3.8.1.TAR.gz
tar -xf WPSV3.8.1.TAR
cd WPS/
./clean
./configure
edit configure.wps e change variable WRF_DIR = /home/user/WRFV3
./compile
```
**UPP:**
```
tar zxvf DTC_upp_v3.2.tar.gz
cd UPPV3.2/
./clean
./configure
edite o configure.upp e ajuste os PATHs do WRF, NETCDF e grib2
./compile
```
