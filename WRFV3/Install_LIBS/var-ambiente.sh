#!/bin/bash

export DIR=/home/atmosmarine/PNT/temp/LIBRARIES
export CC=gcc
export CXX=g++
export FC=gfortran
export FCFLAGS=-m64
export F77=gfortran
export FFLAGS=-m64

export PATH=$DIR/netcdf/bin:$PATH
export NETCDF=$DIR/netcdf

export PATH=$DIR/mpich/bin:$PATH

export LDFLAGS=-L$DIR/grib2/lib 
export CPPFLAGS=-I$DIR/grib2/include

export JASPERLIB=$DIR/grib2/lib

export JASPERINC=$DIR/grib2/include
